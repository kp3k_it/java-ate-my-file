# java-ate-my-file

This is about poor me that lose my file eaten by angry java 
and some Go monitoring service

Learning&building plan (i will edit it in the moments of realizing what nonsense is written below, ha-ha): 

Service in Java: 
 - build Java environment - done # Oracle Java 17
 - learn hello world - done # Hello.class in .gitignore
 - learn some basic operations & syntax
 - learn java webserver (Spring?)
 - learn zookeper (queues) ??? # just thinking

Service in Go:
 - build Go environment - done
 - learn hello world - done
 - learn Prometheus and its java exporters (how to build exporter in Go)

 Docker & Docker compose:
  - learn handling Java&Go in docker
 
 Gitlab:
  - May be some CI for java part?

  Docs: 
 - Gitlab README
 - Comments
 - This plan
